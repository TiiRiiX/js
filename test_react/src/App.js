import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

var my_data = [
  {
    author: "Я",
    text: "Ничего не знаю, ничего не помню",
    big_text: "А можнет я чего-то и знаю, но иногда задумываюсь над своей чем-то и все сразу пропадает"
  },
  {
    author: "Ты",
    text: "Как-то знаю, как-то помню",
    big_text: "А кто-то возможно и все знает, но такие люди зачастую только делают вид, что все знают"
  },
  {
    author: "Lavid",
    text: "Can are you hear me?",
    big_text: "I'm sorry, but I don't know how to work with private data."
  }
];

class Article extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
  }

  myClick() {
    this.setState({visible: true});
  }

  render() {
    var item = this.props.item;
    var visible = this.state.visible;
    return (
      <div className="article">
        <p className="news_author"><h3>{item.author}</h3></p>
        <p className="news_text">{item.text}</p>
        <a href="#" onClick={() => this.myClick()} className={'news_readmore ' + (visible ? 'none' : '')}>Подробнее</a>
        <p className={'news_big_text ' + (visible ? '' : 'none')}>{item.big_text}</p>
      </div>
      );
  }
}

class News extends Component {
  render() {
      var data = this.props.data;
      var news_templates;
      if (data.length > 0) {
        news_templates = data.map(function(item, index) {
          return (
            <div key={index}>
              <Article item={item} />
            </div>
         )
        });
      } else {
        news_templates = <p> Новостей нет</p>;
      }

      return (
        <div className="news">
          {news_templates}
          <strong className={data.length > 0 ? '' : 'none'}> Всего новостей: {data.length} </strong>
        </div>
      );
  }
}

class AddNews extends Component {
  constructor(props) {
    super(props);
    this.myClick = this.myClick.bind(this);
  }

  myClick(event) {
    alert(this.textInpt.value);
  }

  render() {
    return(
      <div>
        <input className="test_input" defaultValue='' ref={(input) => {this.textInpt = input; }} placeholder="Введите что-то"/>
        <button onClick={this.myClick}>Alert</button>
      </div>
      );
  }
}

class Comments extends Component {
  render () {
    return (
      <div className="Comments">
        Нет новостей - комментировать нечего.
      </div>
    );
  }
}

class App extends Component {
  render() {
    return (
      <div className="App">
        <h3> Новости </h3>
        <AddNews />
        <News data={my_data}/>
        <Comments />
      </div>
    );
  }
}

export default App;
