import React, { Component } from 'react';
import './App.css';
import "bootstrap/dist/css/bootstrap.css";
import { NavItem, Nav, Grid, Row, Col, FormControl } from "react-bootstrap";

const CITES = [
	{name: "Perm", zip: "511196"},
	{name: "Moscov", zip: "524901"},
	{name: "Yekaterinburg", zip: "1486209"},
	{name: "Custom city"}
];

class WeatherCity extends Component {
	constructor() {
		super();
		this.state = {
			weatherData: null
		};
		this.componentDidMount = this.componentDidMount.bind(this);
	}
	componentDidMount() {
		if (this.props.index == (CITES.length - 1)) {
			const city = this.refs.city;
			console.log(this.props.textInput);
		} else {
			const zip = this.props.zip;
			const URL = "http://api.openweathermap.org/data/2.5/weather?id=" + zip + "&appid=b1b35bba8b434a28a0be2a3e1071ae5b&units=metric";
			fetch(URL).then(res => res.json()).then(json => {
				this.setState({ weatherData: json });
				});
		}
	}
	render() {
		const weatherData = this.state.weatherData;
		if (!weatherData) return <h3> Loading... </h3>;
		if (weatherData.weather){
			const weather = weatherData.weather[0];
			const iconUrl = "http://openweathermap.org/img/w/" + weather.icon + ".png";
			return (
				<div>
					<h1>
						{weather.main} in {weatherData.name}
						<img src={iconUrl} alt={weatherData.description} />
					</h1>
					<p>Current: {weatherData.main.temp}°</p>
					<p>High: {weatherData.main.temp_max}°</p>
					<p>Low: {weatherData.main.temp_min}°</p>
					<p>Wind Speed: {weatherData.wind.speed} mi/hr</p>
				</div>
			);
		} else {
			return (<h1>City not found </h1>)
		}
	}
}


class App extends Component {
  constructor() {
	super();
	this.state = {
		activePlace: 0
	};
  }
  render() {
	const activePlace = this.state.activePlace;
	return (
		<Grid>
			<Row>
				<Col md={4}>
				<Nav bsStyle="pills" stacked activeKey={activePlace}
					onSelect={index => {
						this.setState({ activePlace: index });
					}}
				>
					{CITES.map((place, index) => (
						<NavItem key={index} eventKey={index}>{place.name}</NavItem>
					))}
				</Nav>
				<FormControl type="text" label="Custom city" placeholder="Enter city" ref={(input) => { this.textInput = input; }}/>
				</Col>
				<Col md={8}>
					<WeatherCity key={activePlace} index={activePlace} zip={CITES[activePlace].zip} textInput={el => this.textInput = el}/>
				</Col>
			</Row>
		</Grid>
	);
  }
}

export default App;
