var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

io.on('connection', function(socket) {
	console.log('new user');
	socket.broadcast.emit('new user');
	socket.on('chat message', function(msg) {
		console.log('message: ' + msg.message + ' from: ' + msg.user);
		io.emit('chat message', msg);
	});
	socket.on('disconnect', function() {
		console.log('user disconnected');
		io.emit('user disconnected');
	});
});

http.listen(3001, function() {
	console.log('listen on *:3001');
});