import React, { Component } from 'react';
import socketIOClient from 'socket.io-client';
import { ListGroup, ListGroupItem, Modal, Button, FormGroup, FormControl } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import './App.css';

class Messages extends Component {
	render() {
		return this.props.messages.map(function(mes, index) {
			if (mes.style) {
				return <ListGroupItem bsStyle={mes.style} key={index} header={mes.user}>{mes.message}</ListGroupItem>
			} else {
				return <ListGroupItem key={index} header={mes.user}>{mes.message}</ListGroupItem>
			}
		}).reverse();
	}
}


class App extends Component {
	constructor() {
		super();
		this.state = {
			ip: "http://localhost:3001",
			name: "",
			messages: [],
			isShowStartModal: true,
			isShowSelectorRoom: false,
			mes_input: ""
		};
		this.sendMessage = this.sendMessage.bind(this);
		this.closeStartDialog = this.closeStartDialog.bind(this);
		this.closeSelectorRoom = this.closeSelectorRoom.bind(this);
		this.handleChangeMessage = this.handleChangeMessage.bind(this);
		this.handleChangeLogin = this.handleChangeLogin.bind(this);
	}
	componentDidMount() {
		const socket = socketIOClient(this.state.ip);
		socket.on('chat message', data => this.addNewMessage(data));
		socket.on('new user', () => this.showNewUser());
		socket.on('user disconnected', () => this.showLeftUser());
		this.setState({socket: socket});
	}
	addNewMessage(data) {
		var messages = this.state.messages;
		messages.push(data)
		this.setState({ messages: messages })
	}
	sendMessage() {
		if (this.state.mes_input.length > 0) {
			this.state.socket.emit('chat message', {user: this.state.name, message: this.state.mes_input, style: ""});
			this.setState({mes_input: ""});
		}
	}
	showNewUser() {
		var messages = this.state.messages;
		messages.push({user: "Новый юзер", message: "Пришел", style: "success"});
		this.setState({ messages: messages })
	}
	showLeftUser() {
		var messages = this.state.messages;
		messages.push({user: "Старый юзер", message: "Left", style: "danger"});
		this.setState({ messages: messages })
	}
	closeStartDialog() {
		this.setState({isShowStartModal: false});
		this.setState({isShowSelectorRoom: true});
	}
	closeSelectorRoom() {
		this.setState({isShowSelectorRoom: false});
	}
	handleChangeMessage(event) {
		this.setState({mes_input: event.target.value});
	}
	handleChangeLogin(event) {
		this.setState({name: event.target.value});
	}
	render() {
		return (
			<div className="App">
					<FormGroup>
						<FormControl 
							placeholder="Enter message..." 
							value={this.state.mes_input} 
							onChange={this.handleChangeMessage} 
							onKeyPress={(target) => {target.charCode == 13 ? this.sendMessage(): 0}}
						/>
						<Button onClick={this.sendMessage}>Send</Button>
					</FormGroup>
					<ListGroup ref={(textarea) => this.textarea = textarea}>
						<Messages messages={this.state.messages} />
					</ListGroup>
					<Modal show={this.state.isShowStartModal} onHide={this.closeStartDialog}>
						<Modal.Header>
							<Modal.Title>What's your name?</Modal.Title>
						</Modal.Header>
						<Modal.Body>
							<FormGroup>
								<FormControl 
									placeholder="Enter your name" 
									value={this.state.name} 
									onChange={this.handleChangeLogin} 
									onKeyPress={(target) => {target.charCode == 13 ? this.closeStartDialog(): 0}}
								/>
							</FormGroup>
						</Modal.Body>
						<Modal.Footer>
							<Button bsStyle="success" onClick={this.closeStartDialog}>Login</Button>
						</Modal.Footer>
					</Modal>
					<Modal show={this.state.isShowSelectorRoom} onHide={this.closeSelectorRoom}>
						<Modal.Header closeButton>
							<Modal.Title>Choose room</Modal.Title>
						</Modal.Header>
						<Modal.Body>
							
						</Modal.Body>
					</Modal>
			</div>
		);
	}
}

export default App;
