var game = new Phaser.Game(window.innerWidth, window.innerHeight, Phaser.CANVAS, '');


var gameState = {
	preload:function() {
		game.load.image('button_zero', 'img/zero.png');
		game.load.image('button_one', 'img/one.png');
		game.load.image('button_two', 'img/two.png');
		game.load.image('button_three', 'img/three.png');
		game.load.image('button_four', 'img/four.png');
		game.load.image('button_five', 'img/five.png');
		game.load.image('button_six', 'img/six.png');
		game.load.image('button_seven', 'img/seven.png');
		game.load.image('button_eight', 'img/eight.png');
		game.load.image('button_nine', 'img/nine.png');
		game.load.image('button_backspace', 'img/backspace.png');
		game.load.image('button_circular', 'img/circular.png');
		game.load.image('button_division', 'img/division.png');
		game.load.image('button_equal', 'img/equal.png');
		game.load.image('button_maths', 'img/maths.png');
		game.load.image('button_minus', 'img/minus.png');
		game.load.image('button_multiply', 'img/multiply.png');
		game.load.image('button_square', 'img/square-root.png');
	},
	create:function() {
		game.number_field = game.add.text(80, 40, 0, {font: '50px Arial', fill: '#ffffff'});

		game.seven_button = game.add.sprite(0, 100, 'button_seven');
		game.seven_button.scale.setTo(game.world.width/4/game.seven_button.width);

		game.eight_button = game.add.sprite(game.seven_button.width, game.seven_button.y, 'button_eight');
		game.eight_button.scale.setTo(game.world.width/4/game.eight_button.width);

		game.nine_button = game.add.sprite(game.seven_button.width * 2, game.seven_button.y, 'button_nine');
		game.nine_button.scale.setTo(game.world.width/4/game.nine_button.width);

		game.four_button = game.add.sprite(game.seven_button.heigh, game.seven_button.x, 'button_four');
		game.four_button.scale.setTo(game.world.width/4/game.four_button.width);

	},
	update:function() {

	}
}

game.state.add('game', gameState);

game.state.start('game');
