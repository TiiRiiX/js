import React, { Component } from 'react';
import './App.css';
import "bootstrap/dist/css/bootstrap.css";
import { Button } from "react-bootstrap";

class App extends Component {
  constructor() {
    super();
    this.state = {
      clicker: 0,
    };
  }
  render() {
    return (
      <div className="App">
        <h1>{this.state.clicker}</h1>
        <Button onClick={() => {this.setState({clicker: this.state.clicker + 1})}}>Нажми</Button>
      </div>
    );
  }
}

export default App;
