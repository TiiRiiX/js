import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';


describe('Initial', () => {
  it('knows that 2 and 2 make 4', () => {
    expect(2 + 2).toBe(4);
  });
  it('renders without crashing', () => {
	  const div = document.createElement('div');
	  const wrapper = shallow(
	    <App />
	  );
	});
});

describe('Functional', () => {
	const wrapper = mount(
		<App />
	);
	const button = wrapper.find('Button');
	const h1 = wrapper.find('h1');
	it('click increment', () => {
		button.simulate('click');
		expect(h1.text()).toBe('1');
	});
	it('random clicks', () => {
		var clicks = Math.floor(Math.random() * (100 - 50 + 1)) + 50;
		for (let i = 0; i < clicks; i++)
			button.simulate('click');
		expect(h1.text()).toBe((clicks + 1).toString());
	});
});


