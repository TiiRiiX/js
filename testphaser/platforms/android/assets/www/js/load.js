var loadState = {
	preload: function() {
		var loadingLabel = game.add.text(80, 150, 'loading...',{font:'30px Courier', fill:'#ffffff'});
		//Loading assets
		game.load.image('logo', 'assets/LogoBlack512.png');
		game.load.image('ground', 'assets/stoneMid.png');
		game.load.image('sky', 'assets/grid_bg.png');
		game.load.image('player', 'assets/standing-up-man.png');
		game.load.image('button', 'assets/restart.png');
	},

	create: function() {
		//System settings
		game.physics.startSystem(Phaser.Physics.ARCADE);
		//Start menu state
		game.state.start('menu');
	}
};